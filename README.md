
* install postgreSQL install https://www.postgresql.org/ 
* your login and password must be admin
* install node.js https://nodejs.org/
* install Moesif Orign & CORS Changer extension for chrome. If you dont have chrome browser, please install it!
* install git https://git-scm.com/
--- 

* Enter pgAdmin
* create BD 'guru99' database
* create table 'account' with columns:

    1. user_id serial primary key
    2. fn  varying 50 NOT NULL
    3. ln character varying 50 NOT NULL
    4. age integer  NOT NULL
    5. ht character varying 50 NOT NULL
---

* clone repository git clone https://fesmofet@bitbucket.org/fesmofet/projectx.git
* enter to projectX folder
* npm install in console
* in project folder run in terminal: node app.js
* open index.html in Chrome
* Done!

---
You can also change path to your database and table in app.js connectionString variable


